#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import certifi
import requests
import subprocess


def main():
    """
        Main function
    """
    try:
        # Try to reach 'https://git.ircad.fr'. It will fail if the certificate is not installed
        test = requests.get('https://git.ircad.fr')

        print('No SSL Error. Adding custom certs to Certifi store is not needed...')

    except requests.exceptions.SSLError as err:
        print('SSL Error. Adding custom certs to Certifi store...')

        # Download the certifiacte for 'ircad.fr'
        download_and_convert_certificate('CA')
        download_and_convert_certificate('CA-lan')
        download_and_convert_certificate('CA-ihu')


def download_and_convert_certificate(certificate_name):
    certificate_crt = "{0}.crt".format(certificate_name);
    certificate_pem = "{0}.pem".format(certificate_name);
    certificate_url = "http://sogo.ircad.fr/{0}".format(certificate_crt);

    # Download the remote certificate
    response = requests.get(certificate_url)
    if response.status_code == 200:

        # Write the file contents in the response to a file specified by certificate_pem
        with open(certificate_crt, 'wb') as certificate_crt_file:
            for chunk in response.iter_content(chunk_size=8192):
                certificate_crt_file.write(chunk)

        # Convert the crt certificate to pem certificate
        subprocess.run(
            ['openssl', 'x509', '-in', certificate_crt, '-out', certificate_pem],
            check=True
        )

        # Read the pem certificate
        with open(certificate_pem, 'rb') as certificate_pem_file:

            # Write back to certifi store
            with open(certifi.where(), 'ab') as certifi_store_file:
                certifi_store_file.write(certificate_pem_file.read())
    else:
        raise Exception('Invalid certificate URL')

    # Try to reach 'https://git.ircad.fr'. It will fail if the certificate is not installed
    test = requests.get('https://git.ircad.fr')
    print('No SSL Error. Adding custom certificates was successful...')


if __name__ == "__main__":
    main()
